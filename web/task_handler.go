package web

import (
	"github.com/gofiber/fiber/v2"
	"net/http"
	"strings"
	"task-finisher/errorhandler"
	"task-finisher/model"
	"task-finisher/service"
)

type TaskHandler struct {
	taskService service.TaskService
}

func (handler *TaskHandler) getTasks(ctx *fiber.Ctx) error {
	status := strings.ToLower(ctx.Query("status"))
	tasks, err := handler.taskService.GetTasks(status)

	if err != nil {
		return writeErrorResponse(ctx, err)
	}
	return writeResponse(ctx, http.StatusOK, tasks)
}

func (handler *TaskHandler) getTask(ctx *fiber.Ctx) error {
	taskId := ctx.Params("task_id")

	task, err := handler.taskService.GetTask(taskId)
	if err != nil {
		return writeErrorResponse(ctx, err)
	}
	return writeResponse(ctx, http.StatusOK, task)
}

func (handler *TaskHandler) createTask(ctx *fiber.Ctx) error {
	var taskCreateReq model.TaskCreateRequest
	err := ctx.BodyParser(&taskCreateReq)
	if err != nil {
		return writeResponse(ctx, http.StatusBadRequest, err.Error())
	}

	task, err := handler.taskService.CreateTask(taskCreateReq)
	if err != nil {
		return writeErrorResponse(ctx, err)
	}
	return writeResponse(ctx, http.StatusCreated, task)
}

func (handler *TaskHandler) updateTask(ctx *fiber.Ctx) error {
	taskId := ctx.Params("task_id")

	var taskUpdateReq model.TaskUpdateRequest
	err := ctx.BodyParser(&taskUpdateReq)
	if err != nil {
		return writeResponse(ctx, http.StatusBadRequest, err.Error())
	}

	task, err := handler.taskService.UpdateTask(taskId, taskUpdateReq)
	if err != nil {
		return writeErrorResponse(ctx, err)
	}
	return writeResponse(ctx, http.StatusOK, task)
}

func (handler *TaskHandler) deleteTask(ctx *fiber.Ctx) error {
	taskId := ctx.Params("task_id")

	_, err := handler.taskService.DeleteTask(taskId)
	if err != nil {
		return writeErrorResponse(ctx, err)
	}
	return writeResponse(ctx, http.StatusNoContent, nil)
}

func writeErrorResponse(c *fiber.Ctx, err error) error {
	if apiErr, isOk := err.(*errorhandler.APIError); isOk {
		return writeResponse(c, apiErr.Status, apiErr.Error())
	}
	return writeResponse(c, http.StatusInternalServerError, err.Error())
}

func writeResponse(c *fiber.Ctx, code int, data interface{}) error {
	c.Status(code)
	if data != nil {
		return c.JSON(data)
	}
	return nil
}
