package errorhandler

import (
	"fmt"
	"net/http"
	"time"
)

type APIError struct {
	Message   string    `json:"message"`
	Status    int       `json:"status"`
	Timestamp time.Time `json:"timestamp"`
}

func (e APIError) Error() string {
	return fmt.Sprintf("status %d: message %s: timestamp %s", e.Status, e.Message, e.Timestamp)
}

func NewAPIError(status int, message string) error {
	return &APIError{
		Message:   message,
		Status:    status,
		Timestamp: time.Now(),
	}
}

func ThrowNotFoundError(message string) error {
	return NewAPIError(http.StatusNotFound, message)
}

func ThrowUnexpectedError(message string) error {
	return NewAPIError(http.StatusInternalServerError, message)
}

func ThrowValidationError(message string) error {
	return NewAPIError(http.StatusUnprocessableEntity, message)
}

func ThrowBadRequestError(message string) error {
	return NewAPIError(http.StatusBadRequest, message)
}
