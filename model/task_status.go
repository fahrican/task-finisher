package model

type TaskStatus string

const (
	OPEN   TaskStatus = "open"
	CLOSED TaskStatus = "closed"
)
