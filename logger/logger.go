package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"sync"
)

var (
	logger *zap.Logger
	once   sync.Once
)

func init() {
	var err error

	config := zap.NewProductionConfig()
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.TimeKey = "timestamp"
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	config.EncoderConfig = encoderConfig

	logger, err = config.Build(zap.AddCallerSkip(1))
	if err != nil {
		panic(err)
		return
	}
}

func InitLogger() {
	once.Do(func() {
		var err error

		config := zap.NewProductionConfig()
		encoderConfig := zap.NewProductionEncoderConfig()
		encoderConfig.TimeKey = "timestamp"
		encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
		config.EncoderConfig = encoderConfig

		logger, err = config.Build(zap.AddCallerSkip(1))
		if err != nil {
			panic(err)
			return
		}
	})
}

func Close() {
	_ = logger.Sync()
}

func GetError(message string, fields ...zap.Field) {
	logger.Error(message, fields...)
}
