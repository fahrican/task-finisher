package repository

import (
	"fmt"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"task-finisher/errorhandler"
	"task-finisher/logger"
	"task-finisher/model"
)

const IdCheck = "id = ?"
const DescriptionCheck = "description = ?"
const UnexpectedDatabaseError = "unexpected error from database"
const InvalidStatusValue = "invalid status value: %v"
const ErrorWhileQueryingTaskTable = "error while querying task table: %v"
const TaskDescriptionAlreadyExists = "task with description '%s' already exists"

type TaskRepositoryDb struct {
	client *gorm.DB
}

func NewTaskRepositoryDb(dbClient *gorm.DB) TaskRepositoryDb {
	return TaskRepositoryDb{dbClient}
}

func (repoDb TaskRepositoryDb) FindAll(status string) ([]model.Task, error) {
	var tasks []model.Task
	var err error
	open := string(model.OPEN)
	closed := string(model.CLOSED)

	if status != open && status != closed && status != "" {
		return nil, repoDb.logAndReturnError(fmt.Sprintf(InvalidStatusValue, status), false)
	}

	if status == "" {
		err = repoDb.client.Find(&tasks).Error
	} else {
		isOpen := status == open
		err = repoDb.client.Where("is_task_open = ?", isOpen).Order("id").Find(&tasks).Error
	}

	if err != nil {
		return nil, repoDb.logAndReturnError(fmt.Sprintf(ErrorWhileQueryingTaskTable, err.Error()), true)
	}

	return tasks, nil
}

func (repoDb TaskRepositoryDb) FindById(taskId string) (*model.Task, error) {
	var task model.Task

	err := repoDb.client.First(&task, IdCheck, taskId).Error

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errorhandler.ThrowNotFoundError("task not found")
		} else {
			return nil, repoDb.logAndReturnError(fmt.Sprintf("error while scanning task by id: %v", err.Error()), true)
		}
	}
	return &task, nil
}

func (repoDb TaskRepositoryDb) DeleteTask(taskId string) (bool, error) {
	err := repoDb.client.Where(IdCheck, taskId).Delete(&model.Task{}).Error
	if err != nil {
		return false, repoDb.logAndReturnError(fmt.Sprintf("error while deleting task: %v", err.Error()), true)
	}
	return true, nil
}
func (repoDb TaskRepositoryDb) Save(task model.Task) (*model.Task, error) {
	doesExist, err := repoDb.doesDescriptionExist(task.Description, "")
	if err != nil {
		return nil, err
	}
	if doesExist {
		return nil, repoDb.logAndReturnError(fmt.Sprintf(TaskDescriptionAlreadyExists, task.Description), false)
	}

	if err := repoDb.client.Create(&task).Error; err != nil {
		return nil, repoDb.logAndReturnError(fmt.Sprintf("error while creating new task: %v", err), true)
	}

	return &task, nil
}

func (repoDb TaskRepositoryDb) Update(taskId string, task model.Task) (*model.Task, error) {
	doesExist, err := repoDb.doesDescriptionExist(task.Description, taskId)
	if err != nil {
		return nil, err
	}
	if doesExist {
		return nil, repoDb.logAndReturnError(fmt.Sprintf(TaskDescriptionAlreadyExists, task.Description), false)
	}

	result := repoDb.client.Model(&task).Where(IdCheck, taskId).Updates(task)
	if err := result.Error; err != nil {
		return nil, repoDb.logAndReturnError(fmt.Sprintf("error while updating task: %v", err), true)
	}

	if result.RowsAffected == 0 {
		return nil, repoDb.logAndReturnError(fmt.Sprintf("no task found with id: '%s'", taskId), true)
	}

	return &task, nil
}

func (repoDb TaskRepositoryDb) doesDescriptionExist(description string, taskId string) (bool, error) {
	var task model.Task
	var err error
	if taskId == "" {
		err = repoDb.client.Where(DescriptionCheck, description).First(&task).Error
	} else {
		err = repoDb.client.Not(IdCheck, taskId).Where(DescriptionCheck, description).First(&task).Error
	}
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return false, repoDb.logAndReturnError(fmt.Sprintf("error while querying task by description: %v", err), true)
	}
	return err != gorm.ErrRecordNotFound, nil
}

func (repoDb TaskRepositoryDb) logAndReturnError(errMsg string, isInternalError bool) error {
	logger.GetError(errMsg)
	if isInternalError {
		return errorhandler.ThrowUnexpectedError(UnexpectedDatabaseError)
	}
	return errorhandler.ThrowBadRequestError(errMsg)
}
