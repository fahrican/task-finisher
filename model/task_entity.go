package model

import (
	"gorm.io/gorm"
)

type Task struct {
	gorm.Model
	Description   string
	IsReminderSet bool
	IsTaskOpen    bool
	TimeInterval  string
	TimeTaken     int
	Priority      Priority
}

func (instance Task) ToDto() TaskFetchResponse {
	return TaskFetchResponse{
		ID:            instance.ID,
		Description:   instance.Description,
		IsReminderSet: instance.IsReminderSet,
		IsTaskOpen:    instance.IsTaskOpen,
		CreatedAt:     instance.CreatedAt,
		UpdatedAt:     instance.UpdatedAt,
		DeletedAt:     instance.DeletedAt,
		TimeInterval:  instance.TimeInterval,
		TimeTaken:     instance.TimeTaken,
		Priority:      instance.Priority,
	}
}
