package service

import "task-finisher/model"

type TaskService interface {
	GetTasks(string) ([]model.TaskFetchResponse, error)
	GetTask(string) (*model.TaskFetchResponse, error)
	CreateTask(model.TaskCreateRequest) (*model.TaskFetchResponse, error)
	UpdateTask(string, model.TaskUpdateRequest) (*model.TaskFetchResponse, error)
	DeleteTask(string) (bool, error)
}
