package repository

import (
	"task-finisher/model"
)

type TaskRepository interface {
	FindAll(string) ([]model.Task, error)
	FindById(string) (*model.Task, error)
	Save(model.Task) (*model.Task, error)
	Update(string, model.Task) (*model.Task, error)
	DeleteTask(string) (bool, error)
	doesDescriptionExist(string, string) (bool, error)
	logAndReturnError(string, bool) error
}
