package model

import (
	"fmt"
	"gorm.io/gorm"
	"task-finisher/errorhandler"
	"time"
)

const MinDescriptionLength = 2
const MaxDescriptionLength = 255

type TaskFetchResponse struct {
	ID            uint           `json:"id"`
	Description   string         `json:"description"`
	IsReminderSet bool           `json:"is_reminder_set"`
	IsTaskOpen    bool           `json:"is_task_open"`
	CreatedAt     time.Time      `json:"created_at"`
	UpdatedAt     time.Time      `json:"updated_at"`
	DeletedAt     gorm.DeletedAt `json:"deleted_at"`
	TimeInterval  string         `json:"time_interval"`
	TimeTaken     int            `json:"time_taken"`
	Priority      Priority       `json:"priority"`
}

type TaskCreateRequest struct {
	Description   string   `json:"description"`
	IsReminderSet bool     `json:"is_reminder_set"`
	IsTaskOpen    bool     `json:"is_task_open"`
	TimeInterval  string   `json:"time_interval"`
	TimeTaken     int      `json:"time_taken"`
	Priority      Priority `json:"priority"`
}

type TaskUpdateRequest struct {
	Description   *string         `json:"description"`
	IsReminderSet *bool           `json:"is_reminder_set"`
	IsTaskOpen    *bool           `json:"is_task_open"`
	UpdatedAt     *time.Time      `json:"updated_at"`
	DeletedAt     *gorm.DeletedAt `json:"deleted_at"`
	TimeInterval  *string         `json:"time_interval"`
	TimeTaken     *int            `json:"time_taken"`
	Priority      *Priority       `json:"priority"`
}

func (instance *TaskCreateRequest) ValidateDescription() error {
	if len(instance.Description) < MinDescriptionLength || len(instance.Description) > MaxDescriptionLength {
		message := fmt.Sprintf("description must be between %q and %q characters", MinDescriptionLength, MaxDescriptionLength)
		return errorhandler.ThrowValidationError(message)
	}
	return nil
}

func (instance *TaskUpdateRequest) ValidateDescription() error {
	if instance.Description != nil {
		if len(*instance.Description) < MinDescriptionLength || len(*instance.Description) > MaxDescriptionLength {
			message := fmt.Sprintf("description must be between %q and %q characters", MinDescriptionLength, MaxDescriptionLength)
			return errorhandler.ThrowValidationError(message)
		}
	}
	return nil
}
