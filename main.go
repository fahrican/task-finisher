package main

import (
	"fmt"
	"log"
	"os"
	"task-finisher/logger"
	"task-finisher/web"
)

func init() {
	sanityCheck()
}

func main() {
	logger.InitLogger()
	defer logger.Close()
	web.StartApp()
}

func sanityCheck() {
	environmentVariables := []string{
		"SERVER_ADDRESS",
		"SERVER_PORT",
		"DB_DRIVER_NAME",
		"DB_USER",
		"DB_PASSWORD",
		"DB_HOST",
		"DB_PORT",
		"DB_NAME",
	}

	for _, envVar := range environmentVariables {
		if os.Getenv(envVar) == "" {
			log.Fatal(fmt.Errorf("environment variable '%s' is missing", envVar))
		}
	}
}
