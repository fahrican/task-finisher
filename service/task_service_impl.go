package service

import (
	"net/http"
	"task-finisher/errorhandler"
	"task-finisher/model"
	"task-finisher/repository"
)

type TaskServiceImpl struct {
	taskRepo repository.TaskRepository
}

func NewTaskService(repo repository.TaskRepository) *TaskServiceImpl {
	return &TaskServiceImpl{taskRepo: repo}
}

func (taskService TaskServiceImpl) GetTasks(status string) ([]model.TaskFetchResponse, error) {
	statusValue := model.TaskStatus(status)
	taskDtos := make([]model.TaskFetchResponse, 0)

	if statusValue != model.OPEN && statusValue != model.CLOSED && status != "" {
		return nil, errorhandler.NewAPIError(http.StatusBadRequest, repository.InvalidStatusValue)
	}

	if statusValue == model.OPEN || statusValue == model.CLOSED {
		status = string(statusValue)
	} else {
		status = ""
	}

	tasks, apiError := taskService.taskRepo.FindAll(status)
	if apiError != nil {
		return nil, apiError
	}
	for _, task := range tasks {
		taskDto := task.ToDto()
		taskDtos = append(taskDtos, taskDto)
	}
	return taskDtos, nil
}

func (taskService TaskServiceImpl) GetTask(id string) (*model.TaskFetchResponse, error) {
	task, apiError := taskService.taskRepo.FindById(id)
	if apiError != nil {
		return nil, apiError
	}
	taskDto := task.ToDto()
	return &taskDto, nil
}

func (taskService TaskServiceImpl) CreateTask(request model.TaskCreateRequest) (*model.TaskFetchResponse, error) {
	if apiError := request.ValidateDescription(); apiError != nil {
		return nil, apiError
	}
	task := model.Task{
		Description:   request.Description,
		IsReminderSet: request.IsReminderSet,
		IsTaskOpen:    request.IsTaskOpen,
		TimeInterval:  request.TimeInterval,
		TimeTaken:     request.TimeTaken,
		Priority:      request.Priority,
	}
	savedTask, apiError := taskService.taskRepo.Save(task)
	if apiError != nil {
		return nil, apiError
	}

	response := savedTask.ToDto()
	return &response, nil
}

func (taskService TaskServiceImpl) UpdateTask(id string, request model.TaskUpdateRequest) (*model.TaskFetchResponse, error) {
	if apiError := request.ValidateDescription(); apiError != nil {
		return nil, apiError
	}

	task, apiError := taskService.taskRepo.FindById(id)
	if apiError != nil {
		return nil, apiError
	}
	if request.Description != nil {
		task.Description = *request.Description
	}
	if request.IsReminderSet != nil {
		task.IsReminderSet = *request.IsReminderSet
	}
	if request.IsTaskOpen != nil {
		task.IsTaskOpen = *request.IsTaskOpen
	}
	if request.TimeInterval != nil {
		task.TimeInterval = *request.TimeInterval
	}
	if request.TimeTaken != nil {
		task.TimeTaken = *request.TimeTaken
	}
	if request.Priority != nil {
		task.Priority = *request.Priority
	}

	updateTask, apiError := taskService.taskRepo.Update(id, *task)
	if apiError != nil {
		return nil, apiError
	}

	response := updateTask.ToDto()
	return &response, nil
}

func (taskService TaskServiceImpl) DeleteTask(id string) (bool, error) {
	_, apiError := taskService.taskRepo.FindById(id)
	if apiError != nil {
		return false, apiError
	}
	_, apiError = taskService.taskRepo.DeleteTask(id)
	if apiError != nil {
		return false, apiError
	}
	return true, nil
}
