package web

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
	"log"
	"os"
	"task-finisher/dbsetup"
	"task-finisher/model"
	"task-finisher/repository"
	"task-finisher/service"
)

const (
	TasksUri       = "/api/v1/tasks"
	TasksUriWithId = TasksUri + "/:task_id"
)

func StartApp() {
	dbConfig := getDBConfigFromEnv()

	dbClient := dbsetup.ConnectDatabase(dbConfig)
	if err := dbClient.AutoMigrate(&model.Task{}); err != nil {
		log.Fatalf("error when calling automigrate: %v", err)
	}

	taskHandler := getTaskHandler(dbClient)

	app := setupRouter(taskHandler)
	startServer(app)
}

func getDBConfigFromEnv() *dbsetup.DbConfig {
	return &dbsetup.DbConfig{
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		Username: os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		DbName:   os.Getenv("DB_NAME"),
		SslMode:  os.Getenv("SSL_MODE"),
	}
}

func getTaskHandler(dbClient *gorm.DB) *TaskHandler {
	taskRepositoryDb := repository.NewTaskRepositoryDb(dbClient)
	return &TaskHandler{service.NewTaskService(taskRepositoryDb)}
}

func setupRouter(taskHandler *TaskHandler) *fiber.App {
	router := fiber.New()

	router.Get(TasksUri, taskHandler.getTasks)
	router.Get(TasksUriWithId, taskHandler.getTask)
	router.Post(TasksUri, taskHandler.createTask)
	router.Patch(TasksUriWithId, taskHandler.updateTask)
	router.Delete(TasksUriWithId, taskHandler.deleteTask)

	return router
}

func startServer(router *fiber.App) {
	serverAddress := os.Getenv("SERVER_ADDRESS")
	serverPort := os.Getenv("SERVER_PORT")
	addressAndPort := fmt.Sprintf("%s:%s", serverAddress, serverPort)

	if err := router.Listen(addressAndPort); err != nil {
		log.Fatalf("error when starting the server: %v", err)
	}
}
